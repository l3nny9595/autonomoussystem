/*
 *  Author: Nick Angermaier, Eric Daab, Lennart Kreutzmann,  Lennard Schimmer
 *  Version: 5.2.1
 *  Date: 2024-03-01
 *
 *  Description:
 *  This Arduino sketch controls a remote-controlled car using a PS3 controller. 
 *  It allows for both manual and autonomous control of the car. The code reads 
 *  the joystick inputs from the PS3 controller and adjusts the speed and direction 
 *  of the car accordingly. Additionally, it autonomously detects and avoids 
 *  obstacles by using an ultrasonic distance measurement.
 *
 */
#include <Ps3Controller.h>

#define EN_1 14
#define IN_1 27
#define IN_2 16
#define EN_2 17
#define IN_3 25
#define IN_4 26
#define STICK_OFFSET_COMPEN_SOFT 20
#define STICK_OFFSET_COMPEN_HARD 70
#define TRIG_PIN 12
#define ECHO_PIN 13

enum Mode {
  MANUAL_MODE,
  AUTONOMOUS_MODE
};

bool modeToggle = 0;

Mode currentMode = MANUAL_MODE; // Start im manuellen Modus

int player = 0;
int battery = 0;

struct motorSpeed_2T {
  uint8_t speed_l;
  uint8_t speed_r;
};

struct stickCoordinates {
  uint8_t x;
  uint8_t y;
};


void setup() {
  Serial.begin(115200);
  pinMode(EN_1, OUTPUT);
  pinMode(IN_1, OUTPUT);
  pinMode(IN_2, OUTPUT);
  pinMode(EN_2, OUTPUT);
  pinMode(IN_3, OUTPUT);
  pinMode(IN_4, OUTPUT);
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);

  /*
  *
  * Recommended tool to find out the MAC adress of the PS3 controller: 
  * SixaxisPairTool
  *
  * https://www.chip.de/downloads/SixaxisPairTool_62114993.html
  *
  */
  Ps3.begin("a2:72:72:12:12:12"); // Replace with your MAC address 
  Ps3.attach(notify);
}

void loop() {
  if (currentMode == MANUAL_MODE) {
    motorSpeed_2T motorSp;
    motorSp = controlCarMain(getAngel(Ps3.data.analog.stick.lx, Ps3.data.analog.stick.ly), getStickSpeed(Ps3.data.analog.stick.lx, Ps3.data.analog.stick.ly));
    if(Ps3.data.analog.stick.ly > 0){
      setMotorsBackwards(motorSp.speed_r, motorSp.speed_l);
    }else{
      setMotors(motorSp.speed_r, motorSp.speed_l);
    }  
  }else if (currentMode == AUTONOMOUS_MODE) {
    autonomousMode();   
  }
  delay(10);
}

// Main function for querying the controller
void notify() {
    //---------------------- Battery events ---------------------
  if( battery != Ps3.data.status.battery ){
    battery = Ps3.data.status.battery;
    Serial.print("The controller battery is ");
    if( battery == ps3_status_battery_charging )      Serial.println("charging");
    else if( battery == ps3_status_battery_full )     Serial.println("FULL");
    else if( battery == ps3_status_battery_high )     Serial.println("HIGH");
    else if( battery == ps3_status_battery_low)       Serial.println("LOW");
    else if( battery == ps3_status_battery_dying )    Serial.println("DYING");
    else if( battery == ps3_status_battery_shutdown ) Serial.println("SHUTDOWN");
    else Serial.println("UNDEFINED");
  }

  if( Ps3.event.button_down.cross ){
    //Serial.println("Started pressing the cross button");
    toggleHandler();
  }
}

// Maby a usless function. You can also take the coordinates directly.
stickCoordinates getStckCoordinates(uint8_t x, uint8_t y){
  stickCoordinates coordinates;
  coordinates.x = x;
  coordinates.y = y;
  return coordinates;
}


//--------Motion functions--------

/*
* A function that filters values when the stick is in its original position. 
* This is used for stick offset compensation.
* 
* @param x, y the x and y coordinates from the stick
*
* @param r the radius around the origin for which x and y coordinates 
*           the values are to be ignored
*/
int stickOffsetCompensator(float x, float y, float r) {
    float radius = r;
    float distanceFromOrigin = sqrt(pow(x, 2) + pow(y, 2));

    if (distanceFromOrigin <= radius) {
        return 0;
    } else {
        return 1;
    }
}

// function that calculates an angle from an x and y position of a stick.
float getAngel(int x, int y){
  int degrees = 0;

  // compensates the offset of the stick.
  if( stickOffsetCompensator(x,y, STICK_OFFSET_COMPEN_SOFT) == 0){
    degrees = 0;
  }else{
    float angle = atan2(y, x);
    // Conversion of the angle
    degrees = int(angle * 180.0 / M_PI);
  }
  return degrees;
}

// get Speed from any Input like the right or left trigger
uint8_t getSpeed(uint8_t speed){
  return speed;
}

// get Speed by calculating the distance from the Stick to the Origin
uint8_t getStickSpeed(int x, int y){
  float speedABS = 0.0;
  if( stickOffsetCompensator(x,y, STICK_OFFSET_COMPEN_HARD) == 0){
    speedABS = 0.0;
  }else{
    speedABS = 2*sqrt(pow(x, 2) + pow(y, 2));
    if(speedABS >= 255 ){
      speedABS = 255;
    }
  }
  return (uint8_t)speedABS;
} 

// Control the the car by direction and speed
motorSpeed_2T controlCarMain(float dir, uint8_t speed) {
  uint8_t speed_r;
  uint8_t speed_l;
  motorSpeed_2T motorSpeeds; // Struct to return 2 values

  // returns the cosine value which is then used to calculate the speed factor for the respective motor
  float speedFactor = cos(dir* PI / 180); // 

  // if there is no direction ther should also be no velocity
  if(dir == 0){
    speed_r = speed;
    speed_l = speed;
  }else{
    // if speedfactor > 0, turn more to the right, else turn more to the left.
    if(speedFactor > 0){
      speed_r = (uint8_t)(speed - speed*speedFactor);
      speed_l = speed;
    }else if(speedFactor < 0){
      speed_r = speed;
      speed_l = (uint8_t)(speed + speed*speedFactor);
    }else{
      speed_r = 0;
      speed_l = 0;
    }
  }
  motorSpeeds.speed_l = speed_l;
  motorSpeeds.speed_r = speed_r;
  return motorSpeeds;
}

void setMotors(int speed_L,int speed_R ) {
  //Serial.println(speed_L);
  //Serial.println(speed_R);
  analogWrite(EN_1, speed_L);
  analogWrite(EN_2, speed_R);
  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);
}

void setMotorsBackwards(int speed_L,int speed_R ) {
  //Serial.println(speed_L);
  //Serial.println(speed_R);
  analogWrite(EN_1, speed_L);
  analogWrite(EN_2, speed_R);
  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, HIGH);
  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, HIGH);
}

long measureDistance() {
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  long duration = pulseIn(ECHO_PIN, HIGH);
  long distance = duration * 0.034 / 2; 
  return distance;
}

void toggleMode() {
  if (Ps3.event.button_down.cross) { // check if x-button was pressed
    currentMode = (currentMode == MANUAL_MODE) ? AUTONOMOUS_MODE : MANUAL_MODE;
  }
}

void autonomousMode() {
  long distance = measureDistance();
  // Stop the car if an obstacle is closer than 20 cm
  if (distance < 20 && distance >0) {
    Serial.println(distance);
    setMotors(0, 0);
    moveBackwards(100,100);
    delay(1000);
    turnLeft(500);
    delay(300);
    //distance = 20;
  } else {
    // Otherwise, drive straight ahead
    setMotors(100, 100); // Set speed
  }
}

// toggles the vehicle mode
void toggleHandler(){
  modeToggle = !modeToggle;
  if(modeToggle == 0){
    currentMode = MANUAL_MODE;
    Serial.println("Manual mode is now active");
  }else{
    currentMode = AUTONOMOUS_MODE;
    Serial.println("Autonomous mode is now active");
  }
}

// Classic functions
void moveForward(int speed_L,int speed_R ) {
  analogWrite(EN_1, speed_L);
  analogWrite(EN_2, speed_R);
  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);

}

void moveBackwards(int speed_L,int speed_R) {
  analogWrite(EN_1, speed_L);
  analogWrite(EN_2, speed_R);
  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, HIGH);
  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, HIGH);
}

// right
void turnRight(int vel) {
  analogWrite(EN_1, vel);
  analogWrite(EN_2, vel);
  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, HIGH);
  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);
}

// left
void turnLeft(int vel) {
  analogWrite(EN_1, vel);
  analogWrite(EN_2, vel);
  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, HIGH);
}
